package br.ucsal.bes20202.testequalidade.escola.business;

public class AlunoBOUnitarioTest {

	/**
	 * 
	 * Caso de teste 1: Verificar se alunos ativos são salvos.
	 * 
	 * Entrada: uma instância de aluno ativo;
	 * 
	 * Saída esperada: como este método é void, para avaliação seu comportamento
	 * você deve verificar se houve chamada ao método salvar da classe AlunoDAO.
	 *
	 * Obs1: lembre-se de que o teste é unitário, ou seja, você deve mocar a classe AlunoDAO.
	 * 
	 * Obs2: NÃO é necessário mocar o método getSituacao().
	 *
	 */
	public void testarSalvamentoAlunoAtivo() {
	}

}
